using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using BattleShip.DAL.DataObjects;
using BattleShip.DAL.DataServices;
using Xamarin.Forms;

namespace BattleShip.BL.ViewModels.Main
{
    public class MainViewModel: BaseViewModel
    {
		public object SelectedCoordinates {
			get => Get<object>();
			private set => Set(value);
		}

		public ObservableCollection<Coordinates> BoardItems {
			get => Get<ObservableCollection<Coordinates>>();
			private set => Set(value);
		}

		public ICommand ShotCommand {
			get {
				return new Command(
					execute: async (parameter) => {

						var coordinates = (Coordinates)parameter;
						await GamePlayService.Service.Shot(coordinates, GamePlayService.Service.Human, GamePlayService.Service.Computer);

						await GamePlayService.Service.ComputerShot();

						var singleArr = GamePlayService.Service.CurrentPlayer.GameBoard.Cast<Coordinates>();

						BoardItems = new ObservableCollection<Coordinates>(singleArr);
					});
			}
		}

		public override async Task OnPageAppearing() {
			State = PageState.Loading;

			GamePlayService.Init();

			var singleArr = GamePlayService.Service.CurrentPlayer.GameBoard.Cast<Coordinates>();

			BoardItems = new ObservableCollection<Coordinates>(singleArr);

			State = PageState.Normal;
		}
    }
}

