using BattleShip.BL.ViewModels.Main;
using Xamarin.Forms;

namespace BattleShip.UI.Pages.Main
{
	public partial class MainPage : BasePage
	{
		public MainPage ()
		{
			InitializeComponent ();
		}

		void OnCollectionViewSelectionChanged(object sender, SelectionChangedEventArgs e) {
			((MainViewModel)ViewModel).ShotCommand.Execute(e.CurrentSelection[0]);
		}
	}
}
