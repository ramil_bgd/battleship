using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BattleShip.Converters {
	public class IntToHexConverter : IValueConverter, IMarkupExtension {

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			switch((int)value) {
				case 0:
					return "#00dbef";
				case 1:
					return "#00b622";
				case 2:
					return "#e60000";
				case 3:
					return "#e6b300";

				default:
					return "#00dbef";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			switch((int)value) {
				case 0:
					return "#00dbef";
				case 1:
					return "#00b622";
				case 2:
					return "#e60000";
				case 3:
					return "#e6b300";

				default:
					return "#00dbef";
			}
		}

		public object ProvideValue(IServiceProvider serviceProvider) {
			return this;
		}
	}
}
