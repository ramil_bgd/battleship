using System;
using BattleShip.DAL.DataObjects;

namespace BattleShip.DAL.Helpers {
	public static class GameHelper
	{
		public static Coordinates[,] InitBoardItems() {
			var arr = new Coordinates[10, 10];

			var isOpen = true;
			while (isOpen) {
				for (var i = 0; i < 10; i++) {
					for (var j = 0; j < 10; j++)
						arr[i, j] = new Coordinates(i,j,0);
				}

				var rand = new Random(Guid.NewGuid().GetHashCode());

				var startcolumn = rand.Next(1, 11);
				var startrow = rand.Next(1, 11);
				int endrow = startrow, endcolumn = startcolumn;
				var orientation = rand.Next(1, 101) % 2;

				if (orientation == 0) {
					endrow += 4;
				}
				else {
					endcolumn += 4;
				}

				if (endrow > 10 || endcolumn > 10) {
					isOpen = true;
					continue;
				}

				arr[startrow, startcolumn].Value = 1;

				if (orientation == 0) {
					for (var i = startrow; i < endrow; i++) {
						arr[i, startcolumn].Value = 1;
					}
				}
				else {
					for (var i = startcolumn; i < endcolumn; i++) {
						arr[startrow, i].Value = 1;
					}
				}

				isOpen = false;
			}

			return arr;
		}
	}
}