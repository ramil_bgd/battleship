using System;
namespace BattleShip.DAL.DataServices {
	public static class GamePlayService {
		public static IGameplayService Service { get; private set; }

		public static void Init() {
			Service = new StubGameplayService();
		}
	}
}
