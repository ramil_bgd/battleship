using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BattleShip.DAL.DataObjects;
using BattleShip.DAL.Helpers;

namespace BattleShip.DAL.DataServices
{
	public class StubGameplayService : IGameplayService
	{
		public Player Human { get; set; }
		public Player Computer { get; set; }

		public Player CurrentPlayer { get; set; }

		public List<Coordinates> ComputerShots { get; set; }

		public StubGameplayService()
		{
			Human = new Player("Human") {
				GameBoard = GameHelper.InitBoardItems()
			};

			Computer = new Player("Computer") {
				GameBoard = GameHelper.InitBoardItems()
			};

			CurrentPlayer = Human;

			var singleArr = Human.GameBoard.Cast<Coordinates>();
			ComputerShots = new List<Coordinates>(singleArr);
		}

		public async Task Shot(Coordinates coordinates, Player from, Player to)
		{
			var shotItem = to.GameBoard[coordinates.Row, coordinates.Column];

			switch(shotItem.Value)
			{
				case (int)OccupationType.Empty:
					to.GameBoard[coordinates.Row, coordinates.Column].Value = (int)OccupationType.Miss;
					from.GameBoard[coordinates.Row, coordinates.Column].Value = (int)OccupationType.Miss;
					break;

				case (int)OccupationType.Ship:
					to.GameBoard[coordinates.Row, coordinates.Column].Value = (int)OccupationType.Hit;
					from.GameBoard[coordinates.Row, coordinates.Column].Value = (int)OccupationType.Hit;
					break;
			}
		}

		public async Task ComputerShot() {

			await Task.Delay(500);

			var rand = new Random(Guid.NewGuid().GetHashCode());
			var coordShotIndex = rand.Next(0, ComputerShots.Count);
			var coordinates = ComputerShots[coordShotIndex];

			var shotItem = Human.GameBoard[coordinates.Row, coordinates.Column];

			await Shot(shotItem, Computer, Human);

			ComputerShots.Remove(coordinates);
		}
	}
}