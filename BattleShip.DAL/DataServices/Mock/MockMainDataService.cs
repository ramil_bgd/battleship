using System.Threading;
using System.Threading.Tasks;
using BattleShip.DAL.DataObjects;

namespace BattleShip.DAL.DataServices.Mock {
	class MockMainDataService: BaseMockDataService, IMainDataService {
		public Task<RequestResult<SampleDataObject>> GetSampleDataObject(CancellationToken ctx) {
			return GetMockData<SampleDataObject>("BattleShip.DAL.Resources.Mock.Main.SampleDataObject.json");
		}
	}
}

