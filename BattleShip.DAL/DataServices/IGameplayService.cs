using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BattleShip.DAL.DataObjects;

namespace BattleShip.DAL.DataServices
{
	public interface IGameplayService
	{
		Player Human { get; set; }
		Player Computer { get; set; }
		Player CurrentPlayer { get; set; }
		List<Coordinates> ComputerShots { get; set; }

		Task Shot(Coordinates coordinates, Player from, Player to);
		Task ComputerShot();
	}
}
