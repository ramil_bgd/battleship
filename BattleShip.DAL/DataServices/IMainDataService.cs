using System.Threading;
using System.Threading.Tasks;
using BattleShip.DAL.DataObjects;

namespace BattleShip.DAL.DataServices {
	public interface IMainDataService {
		Task<RequestResult<SampleDataObject>> GetSampleDataObject(CancellationToken ctx);
	}
}

