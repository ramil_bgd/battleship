using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleShip.DAL.DataObjects {
	public class Player
	{
		public string Name { get; set; }
		public Coordinates[,] GameBoard { get; set; }
		//public Coordinates[,] FiringBoard { get; set; }

		public Player(string name) {
			Name = name;
			/*FiringBoard = new Coordinates[10, 10];
			for (var i = 0; i < 10; i++) {
				for (var j = 0; j < 10; j++)
					FiringBoard[i, j] = new Coordinates(i, j, 0);
			}*/
		}
	}
}
