using System;
using System.ComponentModel;

namespace BattleShip.DAL.DataObjects
{
	public enum OccupationType
	{
		Empty = 0,
		Ship = 1,
		Hit = 2,
		Miss = 3
	}
}
