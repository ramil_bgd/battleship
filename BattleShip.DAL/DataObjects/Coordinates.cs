using System;
namespace BattleShip.DAL.DataObjects
{
	public class Coordinates
	{
		public int Row { get; set; }
		public int Column { get; set; }
		public int Value { get; set; }

		public Coordinates(int row, int column, int value)
		{
			Row = row;
			Column = column;
			Value = value;
		}
	}
}
